function limitFunctionCallCount(n,cb) {
    var i = 0
    function cb(){
        if(!cb)return;
        while(i<n){
            i++
            console.log(i+" calling..")
            cb()
        }
    }
    cb(n)
    return "null";
}

console.log(limitFunctionCallCount(5))

module.exports=limitFunctionCallCount                 

/*function callback(n){
    var i = 0
    while(i<n){
        i++
        console.log(i+" calling..")
        callback()
    }
    
}*/