function counterFactory() {
    var inval=5
    var deval=5
    function increment(){
        incrementval=inval+1
        inval++
        return incrementval
    }
    function decrement(){
        decrementval=deval-1
        deval--
        return decrementval
    }
    return {increment,decrement}
}
let counter=counterFactory()
console.log(counter.increment())
console.log(counter.decrement())
console.log(counter.increment())
console.log(counter.decrement())

module.exports=counterFactory
